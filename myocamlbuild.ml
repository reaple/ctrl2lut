open Ocamlbuild_plugin;;

(* inspired by:
   `http://brion.inria.fr/gallium/index.php/Automatic_Version_Generation' *)
let make_version src dest env _ =
  let cmd =
    Printf.sprintf "let compile_time = %S\nlet compile_host = %S"
      (* (Ocamlbuild_pack.My_unix.run_and_open "date -R" input_line) *)
      (Ocamlbuild_pack.My_unix.run_and_open "date +\"%a, %d %b %Y %T %z\"" input_line)
      (Unix.gethostname ()) in
  Cmd (S [ A"echo"; Quote (Sh cmd); Sh"|";
           A"cat"; P (env src); A"-"; Sh">"; P (env dest) ])
;;

let make_version_rule name src dest =
  rule name ~deps:[src] ~prod:dest (make_version src dest)
;;

dispatch begin function
  | After_rules ->
      make_version_rule "version" "version.ml.in" "src/version.ml";

      (* Use both ml and mli files to build documentation: *)
      rule "ocaml: ml & mli -> odoc"
        ~insert:`top
        ~prod:"%.odoc"
        (* "%.cmo" so that cmis of ml dependencies are already built: *)
        ~deps:["%.ml"; "%.mli"; "%.cmo"]
        begin fun env build ->
          let mli = env "%.mli" and ml = env "%.ml" and odoc = env "%.odoc" in
          let tags =
            (Tags.union (tags_of_pathname mli) (tags_of_pathname ml))
            ++"doc_use_interf_n_implem"++"ocaml"++"doc" in
          let include_dirs = Pathname.include_dirs_of (Pathname.dirname ml) in
          let include_flags =
            List.fold_right (fun p acc -> A"-I" :: A p :: acc) include_dirs [] in
          Cmd (S [!Options.ocamldoc; A"-dump"; Px odoc;
                  T (tags++"doc"++"pp"); S (include_flags);
                  A"-intf"; P mli; A"-impl"; P ml])
        end;

      (* Specifying merge options. *)
      pflag ["ocaml"; "doc"; "doc_use_interf_n_implem"] "merge" (fun s -> S[A"-m"; A s]);

      flag ["ocaml"; "doc"] (S [A "-charset"; A "utf-8" ]);
      (* flag ["ocaml"; "compile"; "native"; "noassert"] (S [A "-noassert" ]); *)
  | _ -> ()
end
