#!/bin/sh

# ------------------------------------------------------------------------------

# defaults

prefix="/usr/local";
native="yes";
byte="yes";
pkgdev="";

# usage

usage () {
    cat <<EOF
usage: $0 [options]
where available options are:
  --prefix <prefix>  Specify installation prefix directory
                     ("$prefix" by default)
  --{enable|disable}-{native|byte}
                     Enable|disable the compilation of native/bytecode versions
                     of the libraries and executables
  --opam-pkgdev-dir <dir>
                     Specify the OPAM pkgdev directory
  --help             Display this help and exit
EOF
    exit 0
}

# utils

warn () { echo "$1" 1>&2; }
err () { echo "$1" 1>&2; exit 1; }
arr () { echo "Missing argument for \`$1'!" 1>&2; usage; exit 1; }
arg () { test "x$2" != "x" && echo "$2" || arr "$1"; }
drg () { d="$(arg "$@")"; test -d "$d" && echo "$d" || \
		 err "Argument for \`$1' should be an existing directory!"; }

# data

pkgdeps="reatk.ctrlNbac";

# arg. parsing

while test $# '>' 0; do
    case "$1" in
	--prefix) prefix="$(arg "$1" "$2")"; shift 2;;
	# --docdir) docdir="$(arg "$1" "$2")"; shift 2;;
	--enable-native) native="yes"; shift;;
	--disable-native) native="no"; shift;;
	--enable-byte) byte="yes"; shift;;
	--disable-byte) byte="no"; shift;;
	--opam-pkgdev-dir) pkgdev="$(drg "$1" "$2")"; shift; shift;;
	-h | --help) usage;;
	*) warn "Ignoring argument \`$1'!"; shift;;
    esac;
done;

# tests

test "x$byte" = "xno" -a "x$native" = "xno" && \
    err "At least one of native or bytecode targets must be enabled!";

# test "x$docdir" = "x" && docdir="$prefix/share/doc";
command -v ocamlfind >/dev/null || err "Missing ocamlfind!";

for p in $pkgdeps; do
    ocamlfind query $p 2>&1 >/dev/null || err "Package \`$p' not found!";
done;

if test "x$pkgdev" != "x"; then
    test -f "$pkgdev/generic.mk" || \
	err "Missing file \`generic.mk' in OPAM package development directory!";
else
    pkgdev="opam-pkgdev";
fi;

# output config file

exec 1>config.mk;
echo "PREFIX = $prefix";
# echo "DOCDIR = $docdir";
echo "ENABLE_NATIVE = $native";
echo "ENABLE_BYTE = $byte";

echo "OPAM_PKGDEV_DIR = $pkgdev";

# ------------------------------------------------------------------------------
