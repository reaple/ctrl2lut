open Format
module Symb = CtrlNbac.Symb
open CtrlNbac.AST
open Butin.AST

(* --- *)

type 'a static_bounds = [ `Lb of 'a | `Ub of 'a | `Range of 'a * 'a ]

(* -------------------------------------------------------------------------- *)

module Error = struct

  (** Potentially localized error messages returned by {!tnode}, {!tfunc},
      {!tpred} and {!tenv} below. *)
  type 'f t =
    | EExp of 'f CtrlNbac.AST.exp
    | EPoly of 'f CtrlNbac.AST.exp
    | ETyp of CtrlNbac.AST.typ

  let pp fmt = let pp = fprintf in function
    | EExp e -> pp fmt "Unsupported@ type@ of@ expression:@ `%a'" print_exp e
    | EPoly e -> pp fmt "Unsupported@ untyped@ expression:@ `%a'" print_exp e
    | ETyp t -> pp fmt "Unsupported@ type:@ `%a'" print_typ t

  type 'f errors =
      {
        mutable err_msgs: ('f option * 'f t) list;
      }

  let make () = { err_msgs = [] }
  let reset es = es.err_msgs <- []
  let chk_errors { err_msgs } = err_msgs <> []
  let iter { err_msgs } f = List.iter f err_msgs
  let sort cmp_flags es =
    es.err_msgs <- List.sort (fun (al, _) (bl, _) -> match al, bl with
      | Some a, Some b -> cmp_flags a b
      | _ -> 0) es.err_msgs
  let push es e = es.err_msgs <- e :: es.err_msgs

end

(* -------------------------------------------------------------------------- *)

(* Fatal errors: *)

(** Raised by [tpred] when the predicate has no useful meaning. *)
exception NonControllableSystem

exception UnsupportedWeavedNode
exception ZeroadicNode

(* -------------------------------------------------------------------------- *)

module type PARAMS = sig
  type flg
  val push_err: (flg option * flg Error.t) -> unit
end

module Make (P: PARAMS) : sig

  type typenv

  val init_typenv_from_node
    : ?int_bounds:int static_bounds
    -> ?flt_bounds:float static_bounds
    -> [< P.flg CtrlNbac.AST.node ]
    -> typenv
  val init_typenv_from_pred
    : ?int_bounds:int static_bounds
    -> ?flt_bounds:float static_bounds
    -> [< P.flg CtrlNbac.AST.pred ]
    -> typenv
  val gen_enum_encoding_extras
    : typenv
    -> phrase list

  val tpred
    : typenv
    -> ?ignore_defaults:bool
    -> string
    -> int
    -> P.flg checked_pred -> phrases
  val tfunc
    : typenv
    -> string
    -> int
    -> P.flg checked_func
    -> phrases
  val tnode
    : typenv
    -> ?output_ctrls:bool
    -> string
    -> P.flg checked_node
    -> phrases
  val tenv
    : typenv
    -> string
    -> P.flg checked_node
    -> phrases

end = struct

  open P
  open Helpers
  open Error

  let err f e d = push_err (f, e); d

  (* --- *)

  let reflag ?flag e = match flag with
    | None -> e
    | Some f -> CtrlNbac.AST.flag f e

  let s2i : symb -> ident = Symb.to_string

  let tiref' ?delap s s' = match delap with
    | Some delap when delap s -> Pre s'
    | _ -> iref s'

  let tiref ?delap ?flag s = match delap with
    | Some delap when delap s -> Pre (s2i s)
    | _ -> iref (s2i s)

  type typenv =
      {
        typdefs: flg CtrlNbac.AST.typdefs;
        type_check_id: ident SMap.t;
        label_test_id: ident SMap.t;
        label_slice: (int -> exp) SMap.t;
        int_bounds: int static_bounds option;
        flt_bounds: float static_bounds option;
      }

  (* module OneHot = struct *)

  let gen_fresh n = if Symb.exists n then Symb.fresh n |> Symb.to_string else n
  let test_name tn l = gen_fresh ("eq_"^Symb.to_string tn^"_"^Symb.to_string l)
  let mem_name tn = gen_fresh ("is_"^ Symb.to_string tn)

  let init_typenv ?int_bounds ?flt_bounds typdefs =
    let label_test_id, label_slice, type_check_id = fold_typdefs
      begin fun tn (EnumDef ll, flag) (lt, lc, tc) ->
        let lt, ll, _ = List.fold_left
          (fun (lt, ll, i) (l, _) ->
            let l = label_symb l in
            let lt = SMap.add l (test_name tn l) lt in
            lt, Symb.to_string l :: ll, i + 1)
          (lt, [], 0) ll
        and tc = SMap.add tn (mem_name tn) tc in
        let ll = List.sort compare ll in
        let lc, _ = List.fold_left (fun (lc, i) l ->
          let lc = SMap.add (Symb.of_string l)
            (fun i' -> if i' = i then tt else ff) lc in
          lc, i + 1)
          (lc, 0) ll
        in
        lt, lc, tc
      end typdefs (SMap.empty, SMap.empty, SMap.empty)
    in
    { typdefs; type_check_id; label_test_id; label_slice;
      int_bounds; flt_bounds }

  let init_typenv_from_node ?int_bounds ?flt_bounds n =
    init_typenv ?int_bounds ?flt_bounds (node_desc n).cn_typs

  let init_typenv_from_pred ?int_bounds ?flt_bounds n =
    init_typenv ?int_bounds ?flt_bounds (pred_desc n).pn_typs

  (* end *)

  type enum_var_encoding_data =
      {
        vars: ident list;
        label2cond: exp SMap.t;
      }

  type gen_data =
      {
        typenv: typenv;
        encoding: enum_var_encoding_data SMap.t;
        assertion: exp;
        env: SSet.t;
      }

  let init_gd typenv =
    { typenv; encoding = SMap.empty; assertion = tt; env = SSet.empty }

  (* let reset_env ({ env } as gd) = { gd with env = SSet.empty } *)

  let gen_vars_encoding'' { typdefs } env tn s =
    let EnumDef ll, _flag = find_typdef tn typdefs in
    let vars, lmap, env = List.fold_left
      (fun (vars, lmap, env) (label, _) ->
        let label = label_symb label in
        let v = Symb.to_string s ^ Symb.to_string label in
        let s = Symb.of_string v in
        let v = if SSet.mem s env then Symb.fresh v else s in
        let env = SSet.add v env in
        s2i v :: vars, SMap.add label (iref (s2i v)) lmap, env)
      ([], SMap.empty, env) ll
    in
    { vars = List.sort compare vars; label2cond = lmap }, env

  let gen_vars_encoding' typenv tn v =
    gen_vars_encoding'' typenv SSet.empty tn v |> fst

  let gen_vars_encoding ({ typenv; env } as gd) tn v =
    let ve, env = gen_vars_encoding'' typenv env tn v in
    let encoding = SMap.add v ve gd.encoding in
    let assertion = IdentRef (SMap.find tn gd.typenv.type_check_id,
                              List.map (fun v -> Exp (iref v)) ve.vars) in
    let assertion = conj assertion gd.assertion in
    ve, { gd with env; encoding; assertion }

  let gen_one_hot_cond vars =
    List.fold_left (fun acc v -> List.fold_left
      (fun acc v' -> let ir = iref v' in conj acc (if v = v' then ir else neg ir))
      tt vars |> disj acc)
      ff vars

  let gen_enum_encoding_extras typenv =
    fold_typdefs begin fun tn (EnumDef ll, flag) phrases ->

      let test_args = gen_vars_encoding' typenv tn (Symb.of_string "v") in
      let test_params = [ of_lst test_args.vars, (`Bool, false) ] in
      let phrases = List.fold_left
        (fun acc (l, _) ->
          let l = label_symb l in
          let test_name = SMap.find l typenv.label_test_id in
          let test_trace = Exp (SMap.find l test_args.label2cond) in
          let test_lb = LetBinding { lb_name = test_name;
                                     lb_params = test_params;
                                     lb_typ_opt = Some `Bool;
                                     lb_trace = test_trace } in
          test_lb :: acc)
        phrases ll
      in

      let mem_name = SMap.find tn typenv.type_check_id in
      let mem_params = test_params in
      let mem_trace = Exp (gen_one_hot_cond test_args.vars) in
      let mem_lb = LetBinding { lb_name = mem_name;
                                lb_params = mem_params;
                                lb_typ_opt = Some `Bool;
                                lb_trace = mem_trace } in

      mem_lb :: phrases
    end typenv.typdefs []

  let vars_encoding { encoding } v = try (SMap.find v encoding).vars with
    | Not_found -> [ s2i v ]

  let pop_decl_vars_assertion ({ assertion } as gd) =
    assertion, { gd with assertion = tt }

  (* --- *)

  let tnnop = function
    | `Sum -> Plus
    | `Sub -> Minus
    | `Mul -> Times
    | `Div -> Slash

  let teqrel = function
    | `Eq -> Eq
    | `Ne -> Ne

  let ttotrel = function
    | #eqrel as r -> teqrel r
    | `Lt -> Lt
    | `Le -> Le
    | `Gt -> Gt
    | `Ge -> Ge

  let op_mapfold o = List.fold_left (fun e f -> BExp (o, e, f))
  let op_bmapfold = function
    | `Conj -> op_mapfold And
    | `Disj -> op_mapfold Or
    | `Excl -> fun a tl -> excl (a :: tl)

  let teeq ?delap gd v l =
    IdentRef (SMap.find l gd.typenv.label_test_id,
              List.map (fun v' -> Exp (tiref' ?delap v v')) (vars_encoding gd v))

  let tveeq ?delap gd v w =
    let vl = vars_encoding gd v and wl = vars_encoding gd w in
    List.fold_left2 (fun acc vi wi -> conj acc (beq (tiref' ?delap v vi)
                                               (tiref' ?delap w wi)))
      tt vl wl

  let tenumcmp ?delap gd ?flag (`Ecmp (o, e, f) as exp) =
    try
      let eq = match deflag e, deflag f with
        | `Ref v, `Enum l | `Enum l, `Ref v -> teeq ?delap gd v (label_symb l)
        | `Ref v, `Ref w -> tveeq ?delap gd v w
        | _ -> raise Exit
      in
      if o = `Eq then eq else neg eq
    with
      | Exit -> err flag (EExp (`Bexp (reflag ?flag exp))) tt

  let tenumin ?delap gd ?flag (`Ein (v, l, tl) as exp) =
    try
      let as_label l = match deflag l with
        | `Enum l -> label_symb l | _ -> raise Exit in
      let v = match deflag v with `Ref v -> v | _ -> raise Exit in
      List.fold_left
        (fun acc l -> disj acc (teeq ?delap gd v (as_label l)))
        (teeq ?delap gd v (as_label l))
        tl
    with
      | Exit -> err flag (EExp (`Bexp (reflag ?flag exp))) tt

  exception SplitEnum of flg eexp

  let rec tb ?delap gd ?flag : 'f CtrlNbac.AST.bexp -> exp =
    let tn = tn ?delap gd in
    let rec tb ?flag : 'f bexp -> exp = function
      | `Ref s -> tiref ?delap ?flag s
      | `Bool true -> tt
      | `Bool false -> ff
      | `Buop (`Neg, e) -> neg (tb e)
      | `Bbop (`Imp, e, f) -> BExp (Impl, tb e, tb f)
      | `Bnop (o, e, f, l) -> op_bmapfold o (tb e) (List.map tb (f::l))
      | `Bcmp (o, e, f) -> BExp (teqrel o, tb e, tb f)
      | `Ncmp (o, e, f) -> BExp (ttotrel o, tn e, tn f)
      | `Ecmp _ as e -> tenumcmp ?delap gd ?flag e
      | `Ein _ as e -> tenumin ?delap gd ?flag e
      | `Pcmp _ as e -> err flag (EPoly (`Bexp (reflag ?flag e))) tt
      | `Ite (c, t, e) -> bite (tb c) (tb t) (tb e)
      | #flag as f -> apply' tb f
      | e -> err flag (EExp (`Bexp (reflag ?flag e))) tt
    in tb ?flag
  and tn ?delap gd ?flag : 'f CtrlNbac.AST.nexp -> exp =
    let tb = tb ?delap gd in
    let rec tn ?flag : 'f nexp -> exp = function
      | `Ref s -> tiref ?delap ?flag s
      | `Int i -> Const (Integer i)
      | `Real f -> Const (Float f)
      | `Mpq m -> Const (Float (Mpqf.to_float m))
      | `Nuop (`Opp, e) -> opp (tn e)
      | `Nnop (o, e, f, l) -> op_mapfold (tnnop o) (tn e) (List.map tn (f::l))
      | `Ite (c, t, e) -> bite (tb c) (tn t) (tn e)
      | #flag as f -> apply' tn f
      | e -> err flag (EExp (`Nexp (reflag ?flag e))) tt
    in tn ?flag
  and sple ?delap gd ?flag i =
    let rec sple ?flag : 'f CtrlNbac.AST.eexp -> exp = function
      | `Ref s -> tiref' ?delap s (List.nth (vars_encoding gd s) i)
      | `Enum l -> (SMap.find (label_symb l) gd.typenv.label_slice) i
      | `Ite (c, t, e) -> bite (tb ?delap gd c) (sple t) (sple e)
      | #flag as f -> apply' sple f
    in sple ?flag
  and texp ?delap gd ?flag : 'f CtrlNbac.AST.exp -> exp = function
    | `Bexp e -> tb ?delap gd ?flag e
    | `Nexp e -> tn ?delap gd ?flag e
    | `Eexp e -> raise (SplitEnum e)    (* XXX Ensure this is always the root. *)
    | (`Ref _ | `Ite _) as e -> err flag (EPoly (reflag ?flag e)) tt
    | #flag as f -> apply' (texp ?delap gd) f

  (* --- *)

  let ttyp gd ?(with_range = false) ?flag = function
    | `Int when with_range -> (match gd.typenv.int_bounds with
        | Some (`Range bnds) -> `BInt bnds | _ -> `Int)
    | `Real when with_range -> (match gd.typenv.flt_bounds with
        | Some (`Range bnds) -> `BReal bnds | _ -> `Real)
    |(`Bool | `Int | `Real) as t -> t
    | t -> err flag (ETyp t) `Bool

  let decl_var gd ?with_range v = function
    | `Enum tn -> let { vars }, gd = gen_vars_encoding gd tn v in
                 of_lst vars, `Bool, gd
    | typ -> sngl (s2i v), ttyp gd ?with_range typ, gd

  let get_var gd v = function
    | `Enum tn -> vars_encoding gd v |> of_lst, `Bool
    | typ -> sngl (s2i v), ttyp gd typ

  let decl_vars'' gd ?with_range =
    List.fold_left
      (fun (il, gd) (v, t) -> let i, t, gd = decl_var gd ?with_range v t in
                           (i, t) :: il, gd)
      ([], gd)
  let get_vars'' gd = List.rev_map (fun (v, t) -> get_var gd v t)

  let decl_vars_c gd vars =
    let tids, gd =
      decl_vars'' gd (List.rev_map (fun (v, t, _) -> v, t) vars) in
    let assertion, gd = pop_decl_vars_assertion gd in
    tids, assertion, gd
  let get_vars' gd vars =
    get_vars'' gd (List.rev_map (fun (v, t, _) -> v, t) vars)

  let _strcmp (v, _) (w, _) = compare (Symb.to_string w) (Symb.to_string v)
  let decl_vars gd ?with_range vars =
    let tids, gd =
      decl_vars'' gd ?with_range (SMap.bindings vars |> List.sort _strcmp) in
    let assertion, gd = pop_decl_vars_assertion gd in
    tids, assertion, gd
  (* let get_vars gd vars = *)
  (*   get_vars'' gd (SMap.bindings vars |> List.sort _strcmp) *)

  let encode_vars gd vars =
    let gd = SMap.fold (fun v t gd -> match t with
      | `Enum tn -> snd @@ gen_vars_encoding gd tn v
      | typ -> gd) vars gd
    in
    pop_decl_vars_assertion gd

  (* --- *)

  let flatten_typed_idents_list' (til: typed_idents_list') =
    List.map (fun (ids, _t) -> (lst ids)) til |> List.flatten |> of_lst

  (* --- *)

  let upper_bound_ids ub ids acc =
    List.fold_left (fun acc v -> conj acc (BExp (Le, iref v, ub))) acc (lst ids)

  let lower_bound_ids lb ids acc =
    List.fold_left (fun acc v -> conj acc (BExp (Le, lb, iref v))) acc (lst ids)

  (* let bound_ids (lb, ub) ids acc = *)
  (*   acc |> lower_bound_ids lb ids |> upper_bound_ids ub ids *)

  let enforce_bounds te (vars: typed_idents_list') =
    let bound_int_ids = match te.int_bounds with
      | Some (`Lb lb) -> lower_bound_ids (csti lb)
      | Some (`Ub ub) -> upper_bound_ids (csti ub)
      | Some _ | None -> fun ids acc -> acc
    and bound_flt_ids = match te.flt_bounds with
      | Some (`Lb lb) -> lower_bound_ids (cstf lb)
      | Some (`Ub ub) -> upper_bound_ids (cstf ub)
      | Some _ | None -> fun ids acc -> acc
    in
    let fold_nums acc =
      List.fold_left (fun acc -> function
        | _, (`Bool | `BInt _ | `BReal _) -> acc         (* type-bound already *)
        | ids, `Int -> bound_int_ids ids acc
        | ids, `Real -> bound_flt_ids ids acc) acc
    in
    fold_nums tt vars |> gen_assert

  let elim_vars s trace = match s with
    | [] -> trace
    | s -> Brace (Exist (of_lst s, trace))           (* braces may be optional *)

  (* --- *)

  let tdefs ?delap gd lst init =
    let letbind s e t = Let ({ lb_name = s; lb_trace = Exp e;
                               lb_params = []; lb_typ_opt = None; }, t) in
    List.fold_left
      (fun t (x, e) -> try letbind (s2i x) (texp ?delap gd e) t with
        | SplitEnum e ->
            fst (List.fold_left
                   (fun (t, i) v -> letbind v (sple ?delap gd i e) t, i + 1)
                   (t, 0) (vars_encoding gd x)))
      init (List.rev lst)

  (* XXX D'rather use type of v, or e, to check that. *)
  let tassign gd ?delap v e =
    try beq (tiref v) (texp ?delap gd e) with
      | SplitEnum e ->
          List.fold_left
            (fun (r, i) v -> conj r (beq (iref v) (sple ?delap gd i e)), i + 1)
            (tt, 0) (vars_encoding gd v) |> fst

  let assigns gd ?delap tspec =
    (* XXX reverse traversal to put error messages in order? *)
    Exp (SMap.fold (fun v e -> conj (tassign gd ?delap v e)) tspec tt)

  let assigns' gd ?delap tspec =
    Exp (List.fold_left (fun acc (v, e) -> conj acc (tassign gd ?delap v e))
           tt tspec)

  let ctrl_name node_name num = node_name ^ "_ctrlr" ^ string_of_int num

  (* --- *)

  let ucg = function
    | [] -> { cnig_input_vars = SMap.empty;
             cnig_contr_vars = SMap.empty;
             cnig_contr_vars' = []; }
    | [ hd ] -> hd
    | _ -> raise UnsupportedWeavedNode

  (* --- *)

  let iog = function
    | [] -> { fnig_input_vars = SMap.empty;
             fnig_output_vars = SMap.empty; }
    | [ hd ] -> hd
    | _ -> raise UnsupportedWeavedNode

  (* --- *)

  let tdefaults ~ignore_defaults ?delap gd c_vars =
    let alt v d = of_lst [ Exp (beq v d); Exp (bne v d) ] in
    let alte ~ascend v tn =
      let EnumDef labels, _ = find_typdef tn gd.typenv.typdefs in
      let { label2cond } = SMap.find v gd.encoding in
      let map = if ascend then List.map else List.rev_map in
      map (fun (l, _) -> let l = label_symb l in
                      Exp (SMap.find l label2cond)) labels |> of_lst
    in
    List.fold_left (fun a -> function
      | (v,`Bool,`None)
      | (v,(`Bool|`Enum _), _) when ignore_defaults -> a
      | (v,`Bool, `Expr d) -> Prio (alt (tiref v) (tb ?delap gd (as_bexp d))) :: a
      | (v,`Bool, `Ascend) -> Prio (alt (tiref v) ff) :: a
      | (v,`Bool, `Descend) -> Prio (alt (tiref v) tt) :: a
      | (v,`Enum tn, `Ascend) -> Prio (alte ~ascend:true v tn) :: a
      | (v,`Enum tn, `Descend) -> Prio (alte ~ascend:false v tn) :: a
      | (v,t, _) -> a)
      [] (List.rev c_vars)

  let tpred te ?(ignore_defaults = false) node_name num (pred: 'f checked_pred)
      : phrases =
    let pi = gather_pred_info pred in
    let gd = init_gd te in
    let ucg = ucg pi.pni_uc_vars in

    let ins, ins_assert, gd = decl_vars gd ucg.cnig_input_vars in
    let outs, outs_assert, gd = match decl_vars_c gd ucg.cnig_contr_vars' with
      | [], _, _ -> raise NonControllableSystem
      | vars, a, gd -> of_lst vars, a, gd in
    let local_assert, gd = encode_vars gd pi.pni_local_vars in

    let trace = match tdefaults ~ignore_defaults gd ucg.cnig_contr_vars' with
      | [] -> Exp (tb gd (pred_desc pred).pn_value)
      | dl -> gen_assert (tb gd (pred_desc pred).pn_value) (Para (of_lst dl))
    in
    let trace = gen_assert local_assert trace in
    let trace = tdefs gd (sorted_pred_definitions pred) trace in
    let trace = mark "predicate" trace in
    let trace = gen_assert outs_assert trace in
    let trace = gen_assert ins_assert trace in
    let trace = enforce_bounds te (lst outs) trace in

    of_lst [
      NodeDecl { ln_name = ctrl_name node_name num;
                 ln_inputs = ins; ln_outputs = outs; ln_trace = trace; };
    ]

  (* --- *)

  let tfunc te node_name num (func: 'f checked_func)
      : phrases =
    let fi = gather_func_info func in
    let gd = init_gd te in
    let iog = iog fi.fni_io_vars in
    let ldefs, odefs = sorted_func_definitions func |>
        List.partition (fun (v, _) -> SMap.mem v fi.fni_local_vars) in

    let ins, ins_assert, gd = decl_vars gd iog.fnig_input_vars in
    let outs, outs_assert, gd = match decl_vars gd iog.fnig_output_vars with
      | [], _, _ -> raise NonControllableSystem
      | vars, a, gd -> of_lst vars, a, gd in
    let local_assert, gd = encode_vars gd fi.fni_local_vars in

    let trace = assigns' gd odefs in
    let trace = gen_assert local_assert trace in
    let trace = tdefs gd ldefs trace in
    let trace = mark "function" trace in
    let trace = gen_assert outs_assert trace in
    let trace = gen_assert ins_assert trace in

    of_lst [
      NodeDecl { ln_name = ctrl_name node_name num;
                 ln_inputs = ins; ln_outputs = outs; ln_trace = trace; };
    ]

  (* --- *)

  let targexp ?delap gd v =
    let v = Symb.of_string v in
    try List.map (tiref' ?delap v) (vars_encoding gd v) with
      | Not_found -> [ tiref ?delap v ]

  let gen_ctrlr_call ?delap node_name num s_vars
      ({ cnig_input_vars = u_vars; cnig_contr_vars' = c_vars })
      gd cont =
    assert (c_vars <> []);
    let args = SMap.fold (fun v _ acc -> s2i v :: acc) s_vars [] in
    let args = SMap.fold (fun v _ acc -> s2i v :: acc) u_vars args in
    let args = List.sort compare args in
    let args = List.map (targexp ?delap gd) args in
    let args = List.flatten args in
    let args = List.map (fun e -> Exp e) args in
    let call = IdentRef (ctrl_name node_name num, args) in
    let ctrls = get_vars' gd c_vars |> flatten_typed_idents_list' in
    (Run (ctrls, call, Some cont), gd)

  let tnode te ?(output_ctrls = false) name (node: 'f checked_node) : phrases =
    let nd = node_desc node and ni = gather_node_info node in
    let gd = init_gd te in
    let s_vars = ni.cni_state_vars in
    let ucg = ucg ni.cni_uc_vars in
    let defs  = sorted_node_definitions node in

    let ins, ins_assert, gd = decl_vars gd ucg.cnig_input_vars in
    let ctrs, ctrs_assert, gd = decl_vars_c gd ucg.cnig_contr_vars' in
    let outs, outs_assert, gd = decl_vars gd s_vars in
    let local_assert, gd = encode_vars gd ni.cni_local_vars in

    let delap = fun v -> SMap.mem v s_vars in
    let assrt = tb ~delap gd nd.cn_assertion in
    let init  = tb gd nd.cn_init in

    let trace = assigns gd ~delap ni.cni_trans_specs in
    let trace = gen_assert assrt trace in    (* not asserted in initial state *)
    let trace = gen_assert local_assert trace in
    let trace = tdefs ~delap gd defs trace in (* assertion may involve locals *)
    let trace, gd = if ucg.cnig_contr_vars' = [] then trace, gd
      else gen_ctrlr_call name 0 ~delap s_vars ucg gd trace in
    let trace = Loop trace |> mark "mainloop" in

    let init  = gen_assert local_assert (Exp init) in
    let init  = tdefs gd defs init in

    let trace = Fby (mark "init" init, trace) in
    let trace = enforce_bounds te ctrs trace in
    let trace, outs' = match output_ctrls with
      | true -> gen_assert ctrs_assert trace, ctrs
      | false -> elim_vars ctrs (gen_assert ctrs_assert trace), []
    in                                     (* elim controllable vars in init. *)
    let trace = gen_assert outs_assert trace in
    let trace = gen_assert ins_assert trace in
    let trace = Fby (trace, Raise "Dead") in

    of_lst [
      ExcDecl (sngl "Dead");
      NodeDecl { ln_name = name;
                 ln_inputs = ins; ln_outputs = (of_lst (outs @ outs'));
                 ln_trace = trace; };
    ]

  (* --- *)

  let strip_defs defs exps =
    let deps = List.fold_left (fun s exp ->
      SSet.union s (exp_dependencies exp)) SSet.empty exps
    in
    let _deps, defs = List.fold_left begin fun (deps, defs) (s, e) ->
      if not (SSet.mem s deps) then (deps, defs)
      else (SSet.union deps (exp_dependencies e), (s, e) :: defs)
    end (deps, []) (List.rev defs) in
    defs

  (* strip unused locals *)
  let encode_def_vars gd all_locals defs =
    let vars = List.fold_left
      (fun acc (v, _) -> SMap.add v (SMap.find v all_locals) acc)
      SMap.empty defs
    in
    encode_vars gd vars

  (** Environment node generation, to feed the given node with inputs satisfying
      the associated assertion. *)
  let tenv te name (node: 'f checked_node) : phrases =
    let nd = node_desc node and ni = gather_node_info node in
    let gd = init_gd te in
    let s_vars = ni.cni_state_vars in
    let ucg = ucg ni.cni_uc_vars in
    let defs = sorted_node_definitions node in
    let defs = strip_defs defs [`Bexp nd.cn_assertion;
                                `Bexp nd.cn_init ] in
    let defl = strip_defs defs [`Bexp nd.cn_assertion] in

    let decl_vars = decl_vars ~with_range:true in
    let ins, ins_assert, gd = decl_vars gd s_vars in
    let outs, outs_assert, gd = decl_vars gd ucg.cnig_input_vars in
    let ctrs, ctrs_assert, gd = decl_vars_c gd ucg.cnig_contr_vars' in
    let local_assert, gd = encode_def_vars gd ni.cni_local_vars defs in
    if outs = [] then raise ZeroadicNode;

    let init  = Exp (tb gd nd.cn_init) in
    let init  = gen_assert ins_assert init in
    let init  = tdefs gd defs init in
    let init  = elim_vars ins init in        (* elim state vars in init state *)
    let init  = mark "init" init in
    let loop  = Loop (Exp (tb gd nd.cn_assertion)) in
    let loop  = mark "mainloop" loop in
    let loop  = gen_assert local_assert loop in
    let loop  = tdefs gd defl loop in
    let loop  = gen_assert ins_assert loop in
    let trace = Fby (init, loop) in
    let trace = gen_assert outs_assert trace in
    let trace = gen_assert ctrs_assert trace in
    let trace = enforce_bounds te ctrs trace in
    let trace = elim_vars ctrs trace in          (* always elim controllables *)
    let trace = enforce_bounds te outs trace in

    of_lst [
      NodeDecl { ln_name = name;
                 ln_inputs = ins; ln_outputs = of_lst outs;
                 ln_trace = trace; };
    ]

end

(* -------------------------------------------------------------------------- *)
