(** Very basic fragment of the Lutin language *)

(* -------------------------------------------------------------------------- *)

type 'a nlist = 'a * 'a list

(* --- *)

type ident = string
type idents = ident nlist

type typ =
  [
  | `Bool
  | `Int
  | `Real
  ]
type typ' =
  [
  | typ
  | `BInt of int * int
  | `BReal of float * float
  ]
type typed_idents = idents * typ'
type typed_idents_list = typed_idents nlist
type typed_idents_list' = typed_idents list
type typed_params = idents * (typ * bool)
type typed_params_list = typed_params nlist
type typed_params_list' = typed_params list

type const =
  | True
  | False
  | Integer of int
  | Float of float

type uop =
  | Neg
  | Opp

type bop =
  | Eq
  | Ne
  | Or
  | Xor
  | And
  | Impl
  | Plus
  | Minus
  | Times
  | Slash
  | Div
  | Mod
  | Lt
  | Le
  | Gt
  | Ge


(** Expressions are untyped, as this module is for generation only. *)
type exp =
  | Const of const
  | IdentRef of ident_ref
  | Pre of ident
  | UExp of uop * exp
  | BExp of bop * exp * exp
  | Cond of exp * exp * exp
and trace =
  | Exp of exp
  | Raise of ident
  | Fby of trace * trace
  | Loop of trace
  | Assert of exp * trace
  (* | ERun of idents * exp * trace *)
  | Run of idents * exp * trace option
  | Exist of typed_idents_list * trace
  | Brace of trace
  | Choice of traces                                    (* no ponderation yet *)
  | Prio of traces
  | Para of traces
  | Let of let_binding * trace
  | Mark of marker * trace
and traces = trace nlist
and traces' = trace list
and ident_ref = ident * traces'                                      (* only? *)
and let_binding =
    {
      lb_name: ident;
      lb_params: typed_params_list';
      lb_typ_opt: typ option;
      lb_trace: trace;
    }
and marker = marker_kind * string
and marker_kind = MrkBrace (* | MrkBeg | MrkEnd *)

type node =
    {
      ln_name: ident;
      ln_inputs: typed_idents_list';
      ln_outputs: typed_idents_list;
      ln_trace: trace;
    }

type phrase =
  | LetBinding of let_binding
  | NodeDecl of node
  | ExcDecl of idents

type phrases = phrase nlist
type prog = phrases

(* -------------------------------------------------------------------------- *)

module Helpers = struct

  exception Empty_list

  let nhd = fun (a, _) -> a
  let ntl = fun (_, b) -> b
  let lst = fun (a, b) -> a :: b
  let of_lst : 'a list -> 'a nlist = function
    | [] -> raise Empty_list
    | a::tl -> (a, tl)
  let sngl x = (x, [])
  let conc (ah, at) (bh, bt) = (ah, at @ (bh :: bt))

  (* --- *)

  let tt = Const True
  let ff = Const False
  let csti i = Const (Integer i)
  let cstf f = Const (Float f)

  let iref i = IdentRef (sngl i)

  let neg = function
    | Const True -> Const False
    | Const False -> Const True
    | UExp (Neg, e) -> e
    | e -> UExp (Neg, e)

  let eq_invol n o = function
    | Const a when a = n -> fun b -> b
    | Const (True | False) -> fun b -> UExp (Neg, b)
    | a -> function
        | Const b when b = n -> a
        | Const (True | False) ->  UExp (Neg, a)
        | b -> BExp (o, a, b)

  let beq = eq_invol True Eq
  let bne = eq_invol False Ne

  let comb2 n o = function
    | Const a when a = n -> fun b -> b
    | Const (True | False) as a -> fun _ -> a
    | a -> function
        | Const b when b = n -> a
        | Const (True | False) as b -> b
        | b -> BExp (o, a, b)

  let conj = comb2 True And
  let disj = comb2 False Or

  let bite = function
    | Const True -> fun t _e -> t
    | Const False -> fun _t e -> e
    | c -> fun t e -> match t, e with
        | Const True, Const False -> c
        | Const False, Const True -> neg c
        | Const True, e -> disj c e
        | Const False, e -> conj (neg c) e
        | t, Const True -> disj (neg c) t
        | t, Const False -> conj c t
        | t, e -> Cond (c, t, e)

  let excl =
    let rec excl (none, one, more) = function
      | [] -> disj none one
      | a::tl ->
          let none = conj none (neg a)
          and one = bite a (neg (disj one more)) one
          and more = disj more (conj one a) in
          excl (none, one, more) tl
    in excl (tt, ff, ff)

  let opp e = UExp (Opp, e)

  let gen_assert b t = match b with
    | Const True -> t
    | a -> Assert (a, t)

  let mark m t = Mark ((MrkBrace, m), t)

end

(* -------------------------------------------------------------------------- *)

open Helpers

let uo2s = function
  | Neg -> "not"
  | Opp -> "-"
let bo2s = function
  | Eq -> "="
  | Ne -> "<>"
  | Or -> "or"
  | Xor -> "xor"
  | And -> "and"
  | Impl -> "=>"
  | Plus -> "+"
  | Minus -> "-"
  | Times -> "*"
  | Slash -> "/"
  | Div -> "div"
  | Mod -> "mod"
  | Lt -> "<"
  | Le -> "<="
  | Gt -> ">"
  | Ge -> ">="

let uop = function
  | Neg -> 10
  | Opp -> 11
let bop = function
  (* else -> 1 *)
  | Impl -> 2
  | Or -> 3
  | Xor -> 4
  | And -> 5
  | Eq | Ne -> 6
  | Lt | Le | Gt | Ge -> 7
  | Plus | Minus -> 8
  | Times | Slash | Div | Mod -> 9

open Format

let pp = fprintf
let ps = pp_print_string
let pmb fmt = pp fmt "(*@@begin_%s@@*)"
let pme fmt = pp fmt "(*@@end_%s@@*)"
let pl ~sep pe fmt = function
  | [] -> ()
  | [a] -> pe fmt a
  | a :: l -> pe fmt a; List.iter (pp fmt "%t%a" sep pe) l
let pnl ~sep pe fmt (nl: 'a nlist) = pl ~sep pe fmt (lst nl)
let pp_comma fmt = pp fmt ",@ "
let pp_semicolon fmt = pp fmt ";@ "

let pi = ps and pir = ps
let pil = pnl ~sep:pp_comma pi
let ptyp fmt = function
  | `Bool -> ps fmt "bool"
  | `Int -> ps fmt "int"
  | `Real -> ps fmt "real"
  | `BInt (l, u) -> pp fmt "int[%d;%d]" l u
  | `BReal (l, u) -> pp fmt "real[%f;%f]" l u
let pptyp fmt = function
  | t, true -> pp fmt "%a@ ref" ptyp t
  | t, false -> pp fmt "%a" ptyp t
let pdecl fmt (il, t) = pp fmt "%a:@ %a" pil il ptyp t
let pparam fmt (il, t) = pp fmt "%a:@ %a" pil il pptyp t
(* let pdecls = pnl ~sep:pp_semicolon pdecl *)
let pphov p fmt = pp fmt "@[<hov>"; pp fmt "%a@]" p
let pdecls = pphov (pnl ~sep:pp_semicolon pdecl)
let pdecls' = pphov (pl ~sep:pp_semicolon pdecl)
let pparams fmt = pphov (pnl ~sep:pp_semicolon pparam) fmt
let pparams' = pphov (pl ~sep:pp_semicolon pparam)
let puo fmt o = ps fmt (uo2s o)
let pbo fmt o = ps fmt (bo2s o)
let ppp p p' fmt =
  if p' > p then pp fmt else (ps fmt "("; kfprintf (fun fmt -> ps fmt ")") fmt)
let rec pe p fmt = function
  | Const True -> ps fmt "true"
  | Const False -> ps fmt "false"
  | Const (Integer i) -> pp fmt "%d" i
  | Const (Float f) -> pp fmt "%f" f
  | IdentRef (r, []) -> pir fmt r
  | IdentRef (r, tl) -> pp fmt "%a (%a)" pir r (pphov ptl) tl
  | Pre i -> pp fmt "pre@ %a" pi i
  | UExp (o,e) -> ppp p (uop o) fmt "%a %a" puo o (peu o) e
  | BExp (o,e,f) -> ppp p (bop o) fmt "%a@ %a@ %a" (peb o) e pbo o (peb o) f
  | Cond (c,t,e) -> ppp p 1 fmt "if@ %a@ then@ %a@ else@ %a" pe0 c pe0 t pe1 e
and peu o fmt = pe (uop o) fmt
and peb o fmt = pe (bop o) fmt
and pe0 fmt = pe 0 fmt
and pe1 fmt = pe 1 fmt
and ptrace fmt = let pe = pe0 in function
  | Exp e -> pe fmt e
  | Raise i -> pp fmt "raise %a" pi i
  | Fby (t1, t2) -> pp fmt "{@\n  @[%a@]@\nfby@\n  @[%a@]@\n}" ptrace t1 ptrace t2
  | Loop t -> pp fmt "@[<2>loop {@\n@[%a@]@]@\n}" ptrace t
  | Assert (e, t) -> pp fmt "@[<2>assert %a@] in@\n%a" pe e ptrace t
  (* | ERun (il, e, t) -> pp fmt "@[<2>erun@ (%a)@ :=@ %a@]@ in@;%a\ *)
  (*                            " pil il pe e ptrace t *)
  | Run (il, e, None) -> pp fmt "@[<2>run (%a)@ :=@ %a@]" pil il pe e
  | Run (il, e, Some t) -> pp fmt "@[<2>run (%a)@ :=@ %a@] in@\n%a\
                                 " pil il pe e ptrace t
  | Exist (dl, t) -> pp fmt "@[<2>exist %a@] in@\n%a" pdecls dl ptrace t
  | Brace t -> pp fmt "{@\n  @[%a@]@\n}" ptrace t
  | Choice tl -> pp fmt "{@[%a@]}" (pnl ~sep:(fun fmt -> pp fmt "@ |@ ") ptrace) tl
  | Prio tl -> pp fmt "{@[%a@]}" (pnl ~sep:(fun fmt -> pp fmt "@ |>@ ") ptrace) tl
  | Para tl -> pp fmt "{@[%a@]}" (pnl ~sep:(fun fmt -> pp fmt "@ &>@ ") ptrace) tl
  | Let (lb, t) -> pp fmt "@[%a in@\n%a@]" plb lb ptrace t
  | Mark ((MrkBrace, m), t) -> pp fmt "@[%a@\n%a@\n%a@]" pmb m ptrace t pme m
and ptl fmt = pl ~sep:pp_comma ptrace fmt
and plb fmt =
  let plb0 fmt { lb_name = n } = pp fmt "%a" pi n
  and plb1 fmt = function
    | { lb_params = [] } -> ()
    | { lb_params = pl } -> pp fmt " (%a)" pparams' pl
  and plb2 fmt = function
    | { lb_typ_opt = None } -> ()
    | { lb_typ_opt = Some typ } -> pp fmt "@ :@ %a" ptyp typ
  and plb3 fmt { lb_trace = t } = ptrace fmt t in
  fun lb -> pp fmt "@[<2>let %a%a%a@ =@;%a@]" plb0 lb plb1 lb plb2 lb plb3 lb
let pn fmt { ln_name; ln_inputs; ln_outputs; ln_trace } =
  pp fmt "@[<2>node %a (%a)@]@;@[<2>returns (%a)@ =@;%a@]"
    ps ln_name pdecls' ln_inputs pdecls ln_outputs ptrace ln_trace

(* --- *)

let print_node = pn
let print_phrase fmt = function
  | LetBinding lb -> plb fmt lb
  | NodeDecl n -> pn fmt n
  | ExcDecl il -> pp fmt "@[<2>exception@ %a@]" pil il
let print_phrases = pnl ~sep:(fun fmt -> pp fmt "@.@.") print_phrase
let print_prog fmt = pp fmt "%a@\n" print_phrases

(* -------------------------------------------------------------------------- *)
