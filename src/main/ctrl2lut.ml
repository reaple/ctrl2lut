open Format
open Filename
open Processing

(* -------------------------------------------------------------------------- *)
(* Options: *)

(** File extensions officially understood by ReaX, with associated input
    types. *)
let ityps_alist = [
  "ctrln", `Ctrln; "cn", `Ctrln;
  "ctrlr", `Ctrlr; "cr", `Ctrlr;
  "ctrlf", `Ctrlf; "cf", `Ctrlf;
  "ctrld", `Ctrld; "cd", `Ctrld;
]

(** Name of official input types as understood by the tool. *)
let ityps = List.map fst ityps_alist

(* --- *)

let set_int_bounds r str = r := Some (ArgScan.int_bounds str)
let set_float_bounds r str = r := Some (ArgScan.float_bounds str)

(* --- *)

let inputs = ref []
let main_output = ref ""
let output_ctrls = ref false
let ignore_defaults = ref false
let main_name = ref "main"

let int_bounds: int Ctrl2But.static_bounds option ref = ref None
let flt_bounds: float Ctrl2But.static_bounds option ref = ref None

let gen_env = ref false
let env_only = ref false
let env_output = ref ""
let env_name = ref "env"

exception Help
let usage = "Usage: ctrl2lut [options] [ -- ] <ctrln> [ <ctrlr> | <ctrlf> ]"
let print_vers () =
  fprintf err_formatter "ctrl2lut-%s (Compiled on %s, %s)@." Version.str
    Version.compile_host Version.compile_time;
  exit 0
let anon x = inputs := x :: !inputs
let u = Arg.Unit (fun _ -> ())
let e s = ("", u, " "^s)
let t s = ("", u, " \ro __"^s^"__")
let h = Arg.Unit (fun _ -> raise Help)
let v = Arg.Unit print_vers
let options = Arg.align
  [
    e""; t"General"; e"";

    ("-o", Arg.Set_string main_output, "<file> ");
    ("--output", Arg.Set_string main_output,
     "<file> Main output file (`-' means standard output)");
    ("--", Arg.Rest anon, " Treat all remaining arguments as input files");

    e""; t"Translation Options"; e"";

    ("-os", Arg.Set output_ctrls, " ");
    ("--output-ctrls", Arg.Set output_ctrls, sprintf
      " Output controllable variables (%B by default)" !output_ctrls);
    ("-m", Arg.Set_string main_name, "<node-name> ");
    ("--main", Arg.Set_string main_name, sprintf
      "<node-name> Set the name of the main node (`%s' by default)" !main_name);
    ("--ignore-defaults", Arg.Set ignore_defaults,
     " Ignore default expressions for controllable variables");
    e(sprintf "(%B by default)" !ignore_defaults);

    ("-ir", Arg.String (set_int_bounds int_bounds), "<range> ");
    ("--int-range", Arg.String (set_int_bounds int_bounds),
     "<range> Specify a range for all local and output integers");

    ("-fr", Arg.String (set_float_bounds flt_bounds), "<range> ");
    ("--float-range", Arg.String (set_float_bounds flt_bounds),
     "<range> Specify a range for all local and output floats");

    e""; t"Environment Generation"; e"";

    ("-e", Arg.Set gen_env, " Enable generation of environment node");
    ("-eo", Arg.Set_string env_output, "<file> ");
    ("--env-output", Arg.Set_string env_output,
     "<file> Environment output file (`-' means standard output)");
    ("--env", Arg.Set_string env_name, sprintf
      "<node-name> Set the name of the environment node (`%s' by default)"
      !env_name);
    ("--env-only", Arg.Set env_only, " Generate environment nodes only");

    e""; t"Miscellaneous"; e"";

    ("-V", v, " ");
    ("--version", v, " Display version info");
    ("-h", h, " ");
    ("-help", h, " ");
    ("--help", h, " Display this list of options");
  ]

let arror f = error ~cont:(fun () -> Arg.usage options usage; exit 1) f

(* -------------------------------------------------------------------------- *)

open Butin.AST.Helpers
module E = Ctrl2But.Error

let es = E.make ()
let report_msgs filename =
  E.sort CtrlNbac.Loc.compare es;                (* Sort messages by location *)
  E.iter es (fun (loc, e) ->
    CtrlNbac.Parser.Reporting.error ~filename ?loc (fun fmt -> E.pp fmt e)
      err_formatter)

module Params = struct
  type flg = CtrlNbac.Loc.t
  let push_err = E.push es
end
module T = Ctrl2But.Make (Params)

(* --- *)

let process read c e err =
  E.reset es;
  let pp = read e in
  report_msgs c;
  err || E.chk_errors es, pp

let get_pred te =
  process (T.tpred te ~ignore_defaults:!ignore_defaults !main_name 0)

let get_func te =
  process (T.tfunc te !main_name 0)

let get_node te =
  process (T.tnode te ~output_ctrls:!output_ctrls !main_name)

let get_env te =
  process (T.tenv te !env_name)

let output_prog ?entity ?override ?(extension=".lut") base te phrases =
  let fmt, oc, name = output_file_from ?entity ~extension ?override base in
  let te = T.gen_enum_encoding_extras te in
  Butin.AST.print_prog fmt (of_lst (te @ lst phrases));
  (match oc with None -> () | Some close -> close ());
  name

let maybe_output_prog ?entity ?override ?extension base te = function
  | None -> None
  | Some e -> output_prog ?entity ?override ?extension base te e

(* --- *)

let main_desc: (unit, formatter, unit) format = "main@ node"
let env_desc: (unit, formatter, unit) format = "environment@ node"

let maybe_report_cmd ?env_file_name main_file_name =
  match main_file_name, env_file_name with
    | Some m, Some e ->
        info "You@ can@ now@ test@ the@ system@ using:@.\
              $ lurettetop -rp 'sut:lutin:%s:%s' -rp 'env:lutin:%s:%s'\
             " m !main_name e !env_name
    | _ -> ()

(* XXX We need two distinct files for main node and environment generation,
   otherwise lutin complains about duplicate definitions of the `Dead'
   exception. *)

let maybe_gen_env cn te node main err =
  let err, env = if not !gen_env && not !env_only then err, None else
      try let err, env = get_env te cn node err in err, Some env with
        | Ctrl2But.ZeroadicNode ->
            warn "Zeroadic@ node:@ skipping@ environment@ generation.";
            err, None
  in
  if err then exit 1;
  let main_file_name =
    if !env_only then None else
      output_prog ~entity:main_desc ~override:!main_output cn te main
  and env_file_name =
    maybe_output_prog ~entity:env_desc ~override:!env_output
      ~extension:"-env.lut" cn te env
  in
  maybe_report_cmd ?env_file_name main_file_name

let init_typenv_from_node node =
  T.init_typenv_from_node ?int_bounds:!int_bounds ?flt_bounds:!flt_bounds node


let handle_cn_cr cn cr =
  let _node_name, node = input_ctrln cn in
  let _pred_name, pred = input_ctrlr cr in
  let te = init_typenv_from_node node in      (* XXX check compat. with pred. *)
  let err = false in
  let err, np = get_node te cn node err in
  let err, prog =
    try let err, pp = get_pred te cr pred err in
        err, (conc pp np)
    with
      | Ctrl2But.NonControllableSystem ->
          warn "Non@ controllable@ system:@ ignoring@ controller.";
          err, np
  in
  maybe_gen_env cn te node prog err

let handle_cn_cf cn cf =
  let _node_name, node = input_ctrln cn in
  let _func_name, func = input_ctrlf cf in
  let te = init_typenv_from_node node in      (* XXX check compat. with func. *)
  let err = false in
  let err, np = get_node te cn node err in
  let err, prog =
    try let err, pp = get_func te cf func err in
        err, (conc pp np)
    with
      | Ctrl2But.NonControllableSystem ->
          warn "Non@ controllable@ system:@ ignoring@ controller@ function.";
          err, np
  in
  maybe_gen_env cn te node prog err

let handle_cn cn =
  let _node_name, node = input_ctrln cn in
  let te = init_typenv_from_node node in
  let err = false in
  let err, np = get_node te cn node err in
  let node_has_ctrls node : bool =
    let open CtrlNbac.AST in
    List.exists (fun { cnig_contr_vars' } -> cnig_contr_vars' <> [])
      (gather_node_info node).cni_uc_vars
  in
  if node_has_ctrls node then
    warn "Main@ node@ features@ controllable@ variables,@ \
          yet@ no@ controller@ was@ given.";
  (* if err then exit 1; *)
  (* let main_file_name = output_prog ~entity:main_desc ~override:!main_output *)
  (*   cn te np in *)
  maybe_gen_env cn te node np err

(* -------------------------------------------------------------------------- *)

let ityp_name = function
  | `Ctrln -> "node"
  | `Ctrlr -> "predicate"
  | `Ctrlf -> "function"
  | `Ctrld -> "controlled node"

let guesstyp filename =
  try List.find (fun (suff, _) -> check_suffix filename suff) ityps_alist |> snd
  with Not_found -> error "Cannot@ guess@ input@ type@ for@ file@ `%s'." filename

let distinguish_n_handle_inputs =
  let try_1 a = match guesstyp a with
    | `Ctrln | `Ctrld -> handle_cn a
    | t -> arror "Nothing@ to@ do@ with@ input@ %s." (ityp_name t)
  and try_2 a b = match guesstyp a, guesstyp b with
    | `Ctrln, `Ctrlr -> handle_cn_cr a b
    | `Ctrlr, `Ctrln -> handle_cn_cr b a
    | `Ctrln, `Ctrlf -> handle_cn_cf a b
    | `Ctrlf, `Ctrln -> handle_cn_cf b a
    | a, b -> arror "Invalid@ combination@ of@ input@ file@ types@ (got@ a@ %s@ \
                    and@ a@ %s)." (ityp_name a) (ityp_name b)
  in
  function
    | [] -> arror "Missing input files."
    | [a] -> try_1 a
    | [a;b] -> try_2 a b
    | _ -> arror "Too many input files."

let main () =
  Arg.parse options anon usage;
  distinguish_n_handle_inputs !inputs;
  exit 0

(* -------------------------------------------------------------------------- *)

let _ = try main () with
  | Help -> Arg.usage options usage; exit 0
  | Ctrl2But.UnsupportedWeavedNode -> error "Unsupported: weaved@ node."
  | (* Failure s | *) Sys_error s -> error "%s" s
  | Exit -> error "aborted."

(* -------------------------------------------------------------------------- *)
