open Format
open Scanf

let try_parse_str str fmts =
  let rec try_parse' = function
    | [] -> None
    | f :: tl -> let ic = Scanning.from_string str in
                try Some (f ic, ic) with
                  | End_of_file | Scan_failure _ -> try_parse' tl
  in
  try_parse' fmts

let try_parse_str name str fmts =
  let open Processing in
  match try_parse_str str fmts with
    | Some (s, ic) when Scanning.end_of_input ic -> s
    | Some _ -> error "spurious@ characters@ at@ end@ of@ %(%)@ `%s'" name str
    | None -> error "syntax@ error@ in@ %(%)@ `%s'" name str

let mk f cstr ic = Scanf.bscanf ic f cstr

(* --- *)

let int_bounds str =
  try_parse_str "integer@ bounds@ specification" str [
    mk " [ %d %_[,:;] %d ] " (fun l u -> `Range (l, u));
    mk " %d  %_[,:;] %d " (fun l u -> `Range (l, u));
    mk " %d: " (fun l -> `Lb l);
    mk " :%d " (fun u -> `Ub u);
    mk " %d " (fun u -> `Ub u);
  ]

let float_bounds str =
  try_parse_str "float@ bounds@ specification" str [
    mk " [ %f %_[,:;]; %f ] " (fun l u -> `Range (l, u));
    mk " %f %_[,:;] %f " (fun l u -> `Range (l, u));
    mk " %f: " (fun l -> `Lb l);
    mk " :%f " (fun u -> `Ub u);
    mk " %f " (fun u -> `Ub u);
  ]
