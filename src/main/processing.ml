open Format
open CtrlNbac.IO

let pp = fprintf

let error ?cont f =
  let maybecont fmt =
    pp_print_newline fmt ();
    match cont with None -> exit 1 | Some c -> c ()
  in
  pp err_formatter "Error: @[";
  kfprintf maybecont err_formatter f
let warn f =
  pp err_formatter "Warning: @[";
  kfprintf (fun fmt -> pp_print_newline fmt ()) err_formatter f
let info f =
  pp err_formatter "Info: @[";
  kfprintf (fun fmt -> pp_print_newline fmt ()) err_formatter f

let abort ?filename n msgs =
  CtrlNbac.IO.report_msgs ?filename err_formatter msgs;
  error "Aborting due to errors in %(%)" n

(* -------------------------------------------------------------------------- *)

let parse_input ?filename p = parse_input ~abort ?filename p

let input_ctrln filename = parse_input ~filename CtrlNbac.Parser.parse_node
let input_ctrlr filename = parse_input ~filename CtrlNbac.Parser.parse_pred
let input_ctrlf filename = parse_input ~filename CtrlNbac.Parser.parse_func

(* -------------------------------------------------------------------------- *)

let output_file_from ?entity ~extension ?override input_file =
  let pp_entity fmt = match entity with | None -> ()
    | Some e -> fprintf fmt e; pp_print_space fmt () in
  let from_file f =
    info "Outputing@ %tinto@ `%s'." pp_entity f;
    let oc = open_out f in
    formatter_of_out_channel oc, Some (fun () -> close_out oc), Some f
  and from_stdout () =
    info "Outputing@ %tonto@ standard@ output." pp_entity;
    std_formatter, None, None
  in
  match override with
    | None | Some "" ->
        (try from_file (Filename.chop_extension input_file ^ extension) with
          | Invalid_argument _ -> from_stdout ())
    | Some "-" -> from_stdout ()
    | Some f -> from_file f

(* -------------------------------------------------------------------------- *)

let straighten_if_tty oc fo cols =
  if Unix.isatty (Unix.descr_of_out_channel oc) then
    pp_set_margin fo cols

let _ =
  let columns = try int_of_string (Sys.getenv "COLUMNS") with _ -> 80 in
  straighten_if_tty stderr err_formatter columns;
  straighten_if_tty stdout std_formatter columns;
  CtrlNbac.Symb.reset ()
